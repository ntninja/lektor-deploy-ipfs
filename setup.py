import ast
import re

from setuptools import setup

_description_re = re.compile(r"description\s+=\s+(?P<description>.*)")

with open("lektor_deploy_ipfs.py", "rb") as f:
	description = str(ast.literal_eval(_description_re.search(
		f.read().decode("utf-8")).group(1)))

setup(
	description=description,
)
